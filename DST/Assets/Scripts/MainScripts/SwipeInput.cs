﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwipeInput : MonoBehaviour
{

    private Vector3 fp;
    private Vector3 lp;
    private float dragDistance;

    void Start()
    {
        dragDistance = Screen.height * 10 / 100;
        Debug.Log("SwipeInput Start called");
    }

    void Update()
    {
        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                fp = touch.position;
                lp = touch.position;
            }
            else if (touch.phase == TouchPhase.Moved)
            {
                lp = touch.position;
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                lp = touch.position;
                if (Mathf.Abs(lp.x - fp.x) > dragDistance || Mathf.Abs(lp.y - fp.y) > dragDistance)
                {
                    if (Mathf.Abs(lp.x - fp.x) > Mathf.Abs(lp.y - fp.y))
                    {
                        if ((lp.x > fp.x))
                        {
                            RightSwipe();
                        }
                        else
                        {
                            LeftSwipe();
                        }
                    }
                }
            }
        }
    }
    public void RightSwipe()
    {
        //what to do when swiped right
        Debug.Log("Swiped Right");
    }
    public void LeftSwipe()
    {
        //what to do when swiped left
        Debug.Log("Swiped Left");
    }
}
