﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager_GameOver01 : MonoBehaviour
{
    public Text Score;
    private void Start()
    {
        Score.text = "made : " + PlayerPrefs.GetInt("Score", 0).ToString();
    }
    public void GotoMenu()
    {
        Application.LoadLevel("MainMenu");
    }
    public void Replay()
    {
        Application.LoadLevel("GamePlay01");
    }
}
