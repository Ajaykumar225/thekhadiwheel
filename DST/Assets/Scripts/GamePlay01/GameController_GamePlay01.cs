﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController_GamePlay01 : MonoBehaviour
{
    private Vector3 fp;
    private Vector3 lp;
    private float dragDistance;

    public Slider Timerslider;
    public Text Instructions;
    private int score;
    public Text Score;

    public Image SwipeIndicator;

    private void Start()
    {
        SwipeIndicator.gameObject.SetActive(false);
        Timerslider.value = 60;
        Instructions.text = "Swipe Right";
        score = 0;
        Score.text ="made : "+ score.ToString();
        StartCoroutine(RunTimer());
    }
    #region Timer
    public IEnumerator RunTimer()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        updateOnTimer();
        StartCoroutine(RunTimer());
    }
    #endregion
    private void updateOnTimer()
    {
        timerDecrement(2);
    }
    private void Update()
    {
        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                fp = touch.position;
                lp = touch.position;
            }
            else if (touch.phase == TouchPhase.Moved)
            {
                lp = touch.position;
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                lp = touch.position;
                if (Mathf.Abs(lp.x - fp.x) > dragDistance || Mathf.Abs(lp.y - fp.y) > dragDistance)
                {
                    if (Mathf.Abs(lp.x - fp.x) > Mathf.Abs(lp.y - fp.y))
                    {
                        if ((lp.x > fp.x))
                        {
                            RightSwipe();
                        }
                        else
                        {
                            LeftSwipe();
                        }
                    }
                }
            }
        }
        if (Timerslider.value==0)
        {
            Application.LoadLevel("GameOver01");
        }
    }
    public void RightSwipe()
    {
        if (Instructions.text == "Swipe Right")
        {
            indicateSwipe(true, true);
            score++;
            updateScore();
            timerIncrement(2);
            createNewInstruction();
        }
        else
        {
            indicateSwipe(false, true);
            timerDecrement(6);
        }
    }
    public void LeftSwipe()
    {
        if (Instructions.text == "Swipe Left")
        {
            indicateSwipe(true, false);
            score++;
            updateScore();
            timerIncrement(1);
            createNewInstruction();
        }
        else
        {
            indicateSwipe(false, false);
            timerDecrement(5);
        }
    }
    void timerDecrement(int value)
    {
        Timerslider.value -= value;
    }
    void timerIncrement(int value)
    {
        Timerslider.value += value;
    }
    void updateScore()
    {
        Score.text = "made : "+ score.ToString();
        PlayerPrefs.SetInt("Score", score);
    }
    void createNewInstruction()
    {
        int random = Random.Range(0, 2);
        bool selector;
        if (random ==0)
            selector = false;
        else
            selector = true;
        if(selector)
        {
            Instructions.text = "Swipe Right";
        }
        else
        {
            Instructions.text = "Swipe Left";
        }
    }
    void indicateSwipe(bool correct,bool direction)
    {
        //StopCoroutine(SwipeClearMain());
        SwipeIndicator.gameObject.SetActive(true);
        if (direction)
            SwipeIndicator.gameObject.GetComponent<RectTransform>().rotation = new Quaternion(0, 0, 0, 0);
        else
            SwipeIndicator.gameObject.GetComponent<RectTransform>().rotation = new Quaternion(0, 1, 0, 0);
        if (correct)
            SwipeIndicator.GetComponent<Image>().color = new Color(0, 255, 0);
        else
            SwipeIndicator.GetComponent<Image>().color = new Color(255, 0, 0);
      
        StartCoroutine(SwipeClearMain());
    }
    IEnumerator SwipeClearMain()
    {
       // while(true)
        //{
            yield return new WaitForSecondsRealtime(0.15f);
            SwipeIndicator.gameObject.SetActive(false);
        //}
    }
}
